<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Actor extends Model
{
    protected $table = "actor";
    protected $primarykey = "actor_id";
    protected $timestamps = false;
}
