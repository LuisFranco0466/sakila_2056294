<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $table="category";
    protected $primaryKey = "category_id";
    public $timestamps = false;

    public function peliculas(){
        return $this->belongsToMany("App\Pelicula",
                                    "film_category",
                                    "film_id", 
                                    "category_id");
    }
}
