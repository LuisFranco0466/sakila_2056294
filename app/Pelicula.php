<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pelicula extends Model
{
    protected $table = "film";
    protected $primaryKey = "film_id";
    public $timestamps = false;

    //extender el modelo para relacionarlo con categorias
    public function categorias(){
        //Relacion muchos a muchos 
        return $this->belongsToMany("App\Categoria",
                                    "film_category",
                                     "category_id",
                                     "film_id");
    }

    public function Idioma(){
        //muchos a uno
        return $this->belongsTo("App\Idioma", "language_id");
    }

}

